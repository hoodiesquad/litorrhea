# frozen_string_literal: true

require File.expand_path('lib/litorrhea/version', __dir__)

Gem::Specification.new do |spec|
  spec.name = 'litorrhea'
  spec.version = Litorrhea::VERSION
  spec.authors = ['Fruit Samurai']
  spec.email = ['hoodiesqd@gmail.com']
  spec.summary = 'Creating and parsing xls/xlsx/csv/txt files.'
  spec.description = 'One gem for any files with tables.'
  spec.homepage = 'https://gitlab.com/hoodiesquad/litorrhea'
  spec.license = 'MIT'
  spec.platform = Gem::Platform::RUBY
  spec.required_ruby_version = '>= 3.0'

  spec.files = Dir[
    'README.md', 'LICENSE', 'CHANGELOG.md',
    'lib/**/*.rb', 'lib/**/*.rake', 'litorrhea.gemspec',
    '.github/*.md', 'Gemfile', 'Rakefile'
  ]

  spec.extra_rdoc_files = ['README.md']

  spec.add_dependency 'rubyzip'
  spec.add_development_dependency 'codecov'
  spec.add_development_dependency 'dotenv'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rubocop', '~> 1.53.0'
  spec.add_development_dependency 'rubocop-performance'
  spec.add_development_dependency 'rubocop-rspec'
  spec.add_development_dependency 'simplecov'
  spec.add_development_dependency 'vcr'
  spec.metadata['rubygems_mfa_required'] = 'true'
end
